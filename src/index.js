import Fastify from 'fastify'
import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

const fastify = Fastify()

fastify.get('/', async (request, reply) => {
  return await prisma.article.findMany({}); 
});

fastify.get('/:newsId', async (request, reply) => {
  const {newsId} = request.params;
  const data = await prisma.article.findFirst({
    where: {
      id: newsId
    }
  }); 
  if (!data) return reply.code(404).send();
  return data;
});

fastify.post('/', async (request, reply) => {
  const {title, content} = request.body;
  if (!title) return reply.code(400).send();
  const data = await prisma.article.create({
    data: {
      title:title,
      content:content??""
    }
  }); 
  if (!data) return reply.code(500).send();
  return data;
});

fastify.put('/:newsId', async (request, reply) => {
  const {newsId} = request.params;
  const {title, content} = request.body;
  if (!title) return reply.code(400).send();
  const data = await prisma.article.findFirst({
    where: {
      id: newsId
    }
  }); 
  if (!data) return reply.code(404).send();
  const update = await prisma.article.update({
    where: {
      id: newsId
    },
    data: {
      title:title,
      content:content??""
    }
  });

  if (!update) return reply.code(500).send();

  return update;
});

fastify.delete('/:newsId', async (request, reply) => {
  const {newsId} = request.params;
  const data = await prisma.article.findFirst({
    where: {
      id: newsId
    }
  }); 
  if (!data) return reply.code(404).send();

  const deleteRow = await prisma.article.delete({
    where: {
      id: newsId
    } 
  });

  if (!deleteRow) return reply.code(500).send();

  return data;
});

fastify.listen({ port: 3000 }, function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
})